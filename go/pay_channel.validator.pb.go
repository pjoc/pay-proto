// Code generated by protoc-gen-gogo. DO NOT EDIT.
// source: pay_channel.proto

package pay

import github_com_mwitkow_go_proto_validators "github.com/mwitkow/go-proto-validators"
import proto "github.com/golang/protobuf/proto"
import fmt "fmt"
import math "math"
import _ "github.com/golang/protobuf/ptypes/timestamp"
import _ "google.golang.org/genproto/googleapis/api/annotations"

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

func (this *ChannelPayRequest) Validate() error {
	if this.Product != nil {
		if err := github_com_mwitkow_go_proto_validators.CallValidatorIfExists(this.Product); err != nil {
			return github_com_mwitkow_go_proto_validators.FieldError("Product", err)
		}
	}
	// Validation of proto3 map<> fields is unsupported.
	return nil
}
func (this *ChannelPayResponse) Validate() error {
	// Validation of proto3 map<> fields is unsupported.
	return nil
}
func (this *NotifyRequest) Validate() error {
	if this.Request != nil {
		if err := github_com_mwitkow_go_proto_validators.CallValidatorIfExists(this.Request); err != nil {
			return github_com_mwitkow_go_proto_validators.FieldError("Request", err)
		}
	}
	return nil
}
func (this *NotifyResponse) Validate() error {
	if this.Response != nil {
		if err := github_com_mwitkow_go_proto_validators.CallValidatorIfExists(this.Response); err != nil {
			return github_com_mwitkow_go_proto_validators.FieldError("Response", err)
		}
	}
	return nil
}
func (this *HTTPRequest) Validate() error {
	// Validation of proto3 map<> fields is unsupported.
	return nil
}
func (this *HTTPResponse) Validate() error {
	// Validation of proto3 map<> fields is unsupported.
	return nil
}
